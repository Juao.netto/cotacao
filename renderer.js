const csv = require('papaparse');

const stocks = {
  oil: 'CL.F',
  gold: 'GC.F',
  silver: 'SI.F',
};

for (let symbol in stocks) {
  const url = `https://stooq.com/q/l/?s=${stocks[symbol]}&f=sd2t2ohlc&h&e=csv`;

  csv.parse(url, {
    download: true,
    delimiter: ',',
    complete: (results) => {
      const prices = results.data[1];

      let previousPrice = 0;
      let currentPrice = 0;

      if (symbol === "oil") {
        previousPrice =  parseFloat(prices[3], 10);
        currentPrice = parseFloat(prices[6], 10);
      } else {
        previousPrice = parseFloat(prices[3], 10) / 28.3495;
        currentPrice = parseFloat(prices[6], 10) / 28.3495;
      }
      
      let change = Math.round((currentPrice - previousPrice) * 100);

      if(symbol === "oil") {
        change = change / 100;
      }
      if (change >= 0) {
        change = `+${change}`;
      }

      const priceElement = document.getElementById(`${symbol}-price`);
      const brPrice = currentPrice.toLocaleString("pt-BR");
      priceElement.innerText = brPrice;

      const changeElement = document.getElementById(`${symbol}-change`);
      changeElement.innerText = change;
    },
  });
}
